<?php
/*
Plugin Name: WP SnowVolleyball FIVB
Plugin URI: https://www.decher.ch/tools
Description: An interface to the FIVB tournament information
Version: 1.0
Author: Dirk
Author URI: https://www.decher.ch
Text Domain: wp-sv-fivb
Domain path: /languages
*/


// check if the wordpress constant is defined, we have a direct call what we don't like
if ( ! defined('ABSPATH' ) ) {
  exit;
}

// include translation
function wp_rating_text_domain() {
  load_plugin_textdomain('wp-sv-fivb', false, plugin_basename( dirname(__FILE__)) . '/languages' );
}
add_action( 'plugins_loaded', 'wp_rating_text_domain');

// include required php files
include('inc/scripts.php');
include('inc/shortcodes.php');