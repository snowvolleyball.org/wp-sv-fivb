<?php

// check if the wordpress constant is defined, we have a direct call what we don't like
if ( ! defined('ABSPATH' ) ) {
  exit;
}

// add the scripts and styles for the front-end
function load_front_scripts () {
  // include front-end css
  // register css
  wp_register_style('wp_sv_fivb_font_css', plugins_url('../assets/css/front.css', __FILE__) );
  // load css
  wp_enqueue_style('wp_sv_fivb_font_css');

  // include front-end scripts
  // register scripts
  wp_register_script('wp_sv_fivb_fivb_umd_js', plugins_url('../assets/js/fivb.umd.js', __FILE__), array() );
  wp_register_script('wp_sv_fivb_polyfill_min_js', plugins_url('../assets/js/polyfill.min.js', __FILE__), array() );
  wp_register_script('wp_sv_fivb_tournament_ranking_js', plugins_url('../assets/js/tournament-ranking.js', __FILE__), array() );
  wp_register_script('wp_sv_fivb_tournament_result_js', plugins_url('../assets/js/tournament-result.js', __FILE__), array() );
  wp_register_script('wp_sv_fivb_tournament_team_list_js', plugins_url('../assets/js/tournament-team-list.js', __FILE__), array() );
  wp_register_script('wp_sv_fivb_webcomponents_bundle_js', plugins_url('../assets/js/webcomponents-bundle.js', __FILE__), array() );
  // load scripts
  wp_enqueue_script('wp_sv_fivb_fivb_umd_js');
  wp_enqueue_script('wp_sv_fivb_polyfill_min_js');
  wp_enqueue_script('wp_sv_fivb_tournament_ranking_js');
  wp_enqueue_script('wp_sv_fivb_tournament_result_js');
  wp_enqueue_script('wp_sv_fivb_tournament_team_list_js');
  wp_enqueue_script('wp_sv_fivb_webcomponents_bundle_js');
}
add_action ('wp_enqueue_scripts', 'load_front_scripts');