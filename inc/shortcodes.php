<?php

// check if the wordpress constant is defined, we have a direct call what we don't like
if ( ! defined('ABSPATH' ) ) {
  exit;
}

function wp_sv_fivb_team_list( $atts, $content = null ) {
  $noTournament = $atts['notournament'];
  return '<tournament-team-list notournament="' . $noTournament . '"></tournament-team-list>';
}
add_shortcode ('wp_sv_fivb_team_list', 'wp_sv_fivb_team_list');

function wp_sv_fivb_result( $atts, $content = null ) {
  $noPhase = $atts['nophase'];
  return '<tournament-result nophase="' .$noPhase . '"></tournament-result>';
}
add_shortcode ('wp_sv_fivb_result', 'wp_sv_fivb_result');

function wp_sv_fivb_ranking( $atts, $content = null ) {
  $noPhase = $atts['nophase'];
  return '<tournament-ranking nophase="' .$noPhase . '"></tournament-ranking>';
}
add_shortcode ('wp_sv_fivb_ranking', 'wp_sv_fivb_ranking');
